# Nozama Project



To Start

Make composer install the dependencies


cd my-project/



composer install



Rename .env-example to .env

In your .env file, uncomment the chosen database url and add in your information


# customize this line!
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"

# to use mariadb:
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=mariadb-10.5.8"

# to use sqlite:
DATABASE_URL="sqlite:///%kernel.project_dir%/var/app.db"

# to use postgresql:
DATABASE_URL="postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8"

# to use oracle:
DATABASE_URL="oci8://db_user:db_password@127.0.0.1:1521/db_name"




Run migrations with the following commands:


php bin/console make:migration 


php bin/console doctrine:migrations:migrate 


