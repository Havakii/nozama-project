<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'string', length: 255)]
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    private $image;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 2)]
    private $price;

    #[ORM\Column(type: 'integer')]
    private $stock;

    #[ORM\ManyToMany(targetEntity: Client::class, mappedBy: 'product_id')]
    private $client_id;

    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'products')]
    private $tag_id;

    public function __construct()
    {
        $this->client_id = new ArrayCollection();
        $this->tag_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return Collection<int, Client>
     */
    public function getClientId(): Collection
    {
        return $this->client_id;
    }

    public function addClientId(Client $clientId): self
    {
        if (!$this->client_id->contains($clientId)) {
            $this->client_id[] = $clientId;
            $clientId->addProductId($this);
        }

        return $this;
    }

    public function removeClientId(Client $clientId): self
    {
        if ($this->client_id->removeElement($clientId)) {
            $clientId->removeProductId($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Category>
     */
    public function getTagId(): Collection
    {
        return $this->tag_id;
    }

    public function addTagId(Category $tagId): self
    {
        if (!$this->tag_id->contains($tagId)) {
            $this->tag_id[] = $tagId;
        }

        return $this;
    }

    public function removeTagId(Category $tagId): self
    {
        $this->tag_id->removeElement($tagId);

        return $this;
    }

    public function __toString() {
        return $this->title;
    }
    
}
