<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class ClientCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Client::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm()->hideOnIndex(),
            TextField::new('firstname', 'First Name'),
            TextField::new('lastname', 'Last Name'),
            TextField::new('email', 'Email'),
            TextField::new('address', 'Address')->hideOnIndex(),
            AssociationField::new('product_id')
        ];
    }

    // public function configureActions(Actions $actions): Actions
    // {
    //     return $actions
    //         ->remove(Crud::PAGE_INDEX, Action::NEW)
    //         ->remove(Crud::PAGE_INDEX, Action::EDIT)
    //         ->add(Crud::PAGE_INDEX, Action::DETAIL)
    //     ;
    // }
    
}
